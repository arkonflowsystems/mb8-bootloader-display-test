//******************************************************************************
//  This file contains test definitions and test management API.
//*****************************************************************************/

#ifndef TEST_H_INCLUDED
#define TEST_H_INCLUDED

// Parameter of test function call
typedef enum
{
    TFP_START,              /* Start/restart of test */
    TFP_PROCESS             /* Process test */

} TestFuncParam;

// Result of test function call
typedef enum
{
    TFR_CONTINUE,           /* Test continues */
    TFR_FINISH              /* Test finished */

} TestFuncResult;

// test function/procedure
typedef TestFuncResult (*TestFunction) (TestFuncParam param);

//------------------------------------------------------------------------------
//  Fw module state init. Call only once in main function before usage of other
//  functions in this module.
//------------------------------------------------------------------------------
void Test_Init(void);

//------------------------------------------------------------------------------
//  Test management task. Call from main loop.
//------------------------------------------------------------------------------
void Test_Task(void);

#endif // TEST_H_INCLUDED
