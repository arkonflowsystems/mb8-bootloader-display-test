//******************************************************************************
//  Buttons driver.
//*****************************************************************************/

#ifndef BUTTONS_H_INCLUDED
#define BUTTONS_H_INCLUDED

// Buttons codes
typedef enum
{
    BTC_NONE = 0xFF,
    BTC_DOWN = 2,
    BTC_UP = 4,
    BTC_LEFT = 3,
    BTC_RIGHT = 1,
    BTC_ESC = 5,
    BTC_ENTER = 0

} ButtonCode;

//------------------------------------------------------------------------------
//  Fw module state init. Call only once in main function before usage of other
//  functions in this module.
//------------------------------------------------------------------------------
void Buttons_Init(void);

//------------------------------------------------------------------------------
//  Scan buttons state. Call periodically.
//------------------------------------------------------------------------------
ButtonCode Buttons_Scan(void);

//------------------------------------------------------------------------------
//  Get button code text representation.
//------------------------------------------------------------------------------
char* Buttons_GetCodeText(ButtonCode buttonCode);

#endif // BUTTONS_H_INCLUDED
