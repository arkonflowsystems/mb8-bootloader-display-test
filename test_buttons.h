//******************************************************************************
//  Buttons test with RTC test.
//*****************************************************************************/

#ifndef TEST_BUTTONS_H_INCLUDED
#define TEST_BUTTONS_H_INCLUDED

#include "test.h"

//------------------------------------------------------------------------------
//  Test function.
//------------------------------------------------------------------------------
TestFuncResult TestButtons_Task(TestFuncParam param);

#endif // TEST_BUTTONS_H_INCLUDED
