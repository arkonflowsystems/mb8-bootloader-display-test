#include "LPC23xx.h"
#include "buttons.h"
#include "maine.h"
#include "display.h"
#include "spi.h"
#include "images.h"
#include "rprintf.h"
#include "string.h"
#include "font.h"
#include "rtc.h"
#include "swi.h"
#include "delay.h"
#include "timer.h"
#include "test.h"
#include <string.h>
#include <stdlib.h>

///**************************************************************************//
///                                 MAIN                                     //
///**************************************************************************//
int main(void)
{
    Buttons_Init();

    SPI_DisplayInit();
    Init_Display();

    DisplaySetContrast(60);
    DISPLAY_LIGHT_IOSET |= DISPLAY_LIGHT_PIN;

    IntEnable();

    RTC_init();         //inicializace RTC
    RTC_set_interrupt();//nastaveni preruseni od RTC

    init_timer1();

    Test_Init();

    ///*******************************************************************************
    ///                                                                            ///
    ///                     HLAVNI SMYCKA                                          ///
    ///                                                                            ///
    ///*******************************************************************************
    while (1)
    {
        Test_Task();
    }

    return 0;
}
