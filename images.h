/****************************************************************************
***  images.c    ************************************************************
*****************************************************************************
*  - Tento modul obsahuje obrazky                                           *
*                                                                           *
*                                                                           *
****************************************************************************/
#ifndef images
#define images

/* Offsety k obrazkum
*  - Offsety je potreba prepocitat, vzdy, pokud odebereme nejaky obrazek.
*  - offset = offset predchoziho + (sirka*vyska)/8 predchoziho + 2.  2=prvni dva byty udavajici rozmery
*/
#define IMG_UNPRES_BTN   (0)    //   9*8   |    11         | 11
#define IMG_PRESS_BTN    (11)   //   9*8   |    11         | 22

extern const unsigned char P_Images[];

#endif //images
