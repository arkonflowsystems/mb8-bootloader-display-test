//******************************************************************************
//  This file contains test management.
//*****************************************************************************/

#include "test.h"
#include "display.h"
#include "test_display.h"
#include "test_charset.h"
#include "test_buttons.h"

// testy
static const TestFunction testFunctions[] =
{
    TestDisplay_Task,
    TestCharset_Task,
    TestButtons_Task,
    0
};

// ukazatel na aktualni test
static TestFunction *pTestFunction = (TestFunction*)&testFunctions[0];

// parametry testu
static TestFuncParam testFuncParam = TFP_START;

//------------------------------------------------------------------------------
//  Fw module state init. Call only once in main function before usage of other
//  functions in this module.
//------------------------------------------------------------------------------
void Test_Init(void)
{
    pTestFunction = (TestFunction*)&testFunctions[0];
    testFuncParam = TFP_START;

    WriteErase();
    WriteCenter(3, "Start of test...");
    WriteNow();
}

//------------------------------------------------------------------------------
//  Test management task. Call from main loop.
//------------------------------------------------------------------------------
void Test_Task(void)
{
    if (*pTestFunction != 0)
    {
        TestFuncResult result = (*pTestFunction)(testFuncParam);
        testFuncParam = TFP_PROCESS;

        if (result == TFR_CONTINUE)
            return;

        /* Test finished, next test */
        testFuncParam = TFP_START;
        pTestFunction++;

        if (*pTestFunction == 0)
        {
            WriteErase();
            WriteCenter(3, "End of test");
            WriteNow();
        }
    }
}

