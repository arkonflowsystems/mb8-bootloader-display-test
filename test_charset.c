//******************************************************************************
//  Character set test.
//*****************************************************************************/

#include "test_charset.h"
#include "display.h"
#include "delay.h"

#define TIME_COUNT  10

#define CHARSET_STR_LGT     13
#define CHARSET_BUF_SIZE    (CHARSET_STR_LGT + 1)
#define MIN_CHAR_CODE       33
#define MAX_CHAR_CODE       126

// test time counter
static unsigned int timeCounter;

//------------------------------------------------------------------------------
//  Test function.
//------------------------------------------------------------------------------
TestFuncResult TestCharset_Task(TestFuncParam param)
{
    if (param == TFP_START)
    {
        WriteErase();

        char buf[CHARSET_BUF_SIZE];
        unsigned char charCount = 0;
        unsigned char line = 0;
        char ch;
        for (ch = MIN_CHAR_CODE; ch <= MAX_CHAR_CODE; ch++)
        {
            buf[charCount++] = ch;

            if (charCount == CHARSET_STR_LGT)
            {
                buf[charCount] = 0;
                Write(0, line, buf);

                charCount = 0;
                line++;
            }
        }

        if (charCount > 0)
        {
            buf[charCount] = 0;
            Write(0, line, buf);
        }

        WriteNow();

        timeCounter = 0;

        return TFR_CONTINUE;
    }
    else
    {
        delay_100ms();

        if (++timeCounter < TIME_COUNT)
            return TFR_CONTINUE;
        else
            return TFR_FINISH;
    }
}
